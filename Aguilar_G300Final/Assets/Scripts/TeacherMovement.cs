﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class TeacherMovement : MonoBehaviour
{
    [SerializeField] protected Vector3 m_from = new Vector3(0.0f, 45.0f, 0.0f);
    [SerializeField] protected Vector3 m_to = new Vector3(0.0f, -45.0f, 0.0f);
    [SerializeField] protected float m_frequency = 1.0f;
    public bool isScanning;
    public float lookingAwayLength;
    public float scanningLength;
    public float timer;
    public TextMeshProUGUI timerText;
    public GameObject pOV;
    public double changenum;
    private Vector3 dir = Vector3.left;
    private float speed = 0.3f;
    public GameObject cone;
    public GameEnding gameEnding;
    public AudioSource scan;
    public AudioSource blah;
    public AudioSource turnGrunt;
    public PlayerMovement playerMovement;







    // Start is called before the first frame update
    void Start()
    {
        isScanning = true;
        
        scanningLength = 10.0f;
        lookingAwayLength  = 15.0f;
    }

    
    // Update is called once per frame
    protected virtual void Update()
    {
        SetTimerText();
        
        if (isScanning)
        {
            SpinMove();
            pOV.SetActive(true);
            cone.SetActive(true);

            if (playerMovement.isStart == true)
            {
                blah.Stop();

                if (!scan.isPlaying)
                {
                    scan.Play();
                }
                
                    
                
            }

        }
        if (!isScanning)
        {
            transform.Translate(dir * speed * Time.deltaTime);

            if (playerMovement.isStart == true)
            {
                scan.Stop();
                if (!blah.isPlaying)
                {
                    blah.Play();
                }
            }


            if (transform.position.x <= -3f)
            {
                dir = Vector3.right;

            }
            else if (transform.position.x >= 0.5f)
            {
                dir = Vector3.left;
                
            }
        }

        timer -= Time.deltaTime;

        if( timer < 0f )
        {
            isScanning = !isScanning;
            if (isScanning)
            {
                timer = scanningLength;
                
            }
            else
            {
                FaceBoard();
                timer = lookingAwayLength;
                pOV.SetActive(false);
                
                lookingAwayLength = 20;
                


            }

        }

    }

    void SpinMove()
    {
        Quaternion from = Quaternion.Euler(this.m_from);
        Quaternion to = Quaternion.Euler(this.m_to);

        float lerp = 0.5f * (1.0f + Mathf.Sin(Mathf.PI * Time.realtimeSinceStartup * this.m_frequency));

        this.transform.localRotation = Quaternion.Lerp(from, to, lerp);

    }


    void FaceBoard()
    {
        var rotationVector = transform.rotation.eulerAngles;
        rotationVector.y = 0;
        transform.rotation = Quaternion.Euler(rotationVector);
        cone.SetActive(false);
        


    }

    void SetTimerText()
    {
       changenum = System.Math.Round(timer, 2);
        if (isScanning)
        {
            timerText.text = ("Scanning: " + changenum);
        }
        if (!isScanning)
        {
            
            timerText.text = ("Teaching: " + changenum);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Lewis"))
        {

            gameEnding.SnitchPlayer();

        }
    }
} 

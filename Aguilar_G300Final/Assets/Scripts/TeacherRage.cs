﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeacherRage : MonoBehaviour
{
    public GameEnding gameEnding;
    public Slider slider;
    public int health = 0;
    public TeacherMovement teacherMovement;

    public Renderer teachrender;
    public Material matTeacher1;
    public Material matTeacher2;
    public Material matTeacher3;
    public Material matTeacher4;

    public AudioSource mad1;
    public AudioSource mad2;
    public AudioSource mad3;
    public AudioSource mad4;



    public void TakeDamage(int damageAmount)
    {
        health += damageAmount;

        if (health >= 50)
        {
            Time.timeScale = .5f;
            gameEnding.PassGrade();
        }
    }


     void Update()
    {
        if (health >= 10)
        {
            Material mats = new Material(matTeacher2);
            teachrender.material = mats;
            
        }

        if (health >= 30)
        {
            Material mats = new Material(matTeacher3);
            teachrender.material = mats;
        }
        if (health >= 45)
        {
            Material mats = new Material(matTeacher4);
            teachrender.material = mats;
        }
        if (health == 10)
        {
            mad1.Play();
        }
        if (health == 30)
        {
            mad2.Play();
        }
        if (health == 45)
        {
            mad3.Play();
        }
        if (health == 50)
        {
            mad4.Play();
        }
        




    }
    public void SetSlider()
    {
        slider.value = health;
    }
    void Start()
    {
        SetSlider();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISelected : MonoBehaviour
{
    public int selectedUI = 0;
    PlayerMovement playerMovement;
    public AudioSource pswitch;
    // Start is called before the first frame update
    void Start()
    {
        SelectUI();
    }

    // Update is called once per frame
    void Update()
    {
       
        
        {
            int previousSelectedUI = selectedUI;
            if (Input.GetKeyDown(KeyCode.E))

            {
                if (selectedUI >= transform.childCount - 1)
                    selectedUI = 0;
                else
                    selectedUI++;
                pswitch.Play();
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (selectedUI <= 0)
                    selectedUI = transform.childCount - 1;
                else
                    selectedUI--;
                pswitch.Play();
            }

            if (previousSelectedUI != selectedUI)
            {
                SelectUI();
            }
        }
    }

    void SelectUI()
    {
        int i = 0;
        foreach (Transform uI in transform)
        {
            if (i == selectedUI)
                uI.gameObject.SetActive(true);
            else
                uI.gameObject.SetActive(false);
            i++;
        }
    }

}

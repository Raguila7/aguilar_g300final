﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SittingArea : MonoBehaviour
{

    public PlayerMovement playerMovement;
    
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "Lewis")
        {
            playerMovement.inArea = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform.name == "Lewis")
        {
            playerMovement.inArea = false;
            playerMovement.Invoke("FButton", 2f);
        }
    }
}

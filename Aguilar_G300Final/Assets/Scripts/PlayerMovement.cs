﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Cinemachine;


public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public float turningSpeed = 50f;

    public GameObject paperballPrefab;
    public GameObject bookPrefab;
    public Transform seatedTransform;
    public GameObject fCanvas;

    public AudioSource throwball;
    public AudioSource paperPU;
    public AudioSource bookPU;
    public AudioSource walk;
    public AudioSource music;
    public AudioSource squeak;



    public TextMeshProUGUI ballcountText;
    public TextMeshProUGUI bookcountText;
    public CinemachineVirtualCamera m_camera;
    public Renderer seatrender;
    public Transform looker;
    public Transform head;
    public GameObject panel;
    public GameEnding gameEnding;
    public TeacherMovement teacherMovement;



    public Material InSeat;
    public Material OutSeat;

    AudioSource m_AudioSource;
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;



    private int pballCount;
    public int bookCount;
    private int tCount;


    public float shotSpeed = 10f;


    public Transform shotSpawn;
    public Transform shotSpawn2;
    public bool isSeated = true;
    public bool inArea = true;
    public bool isStart = false;


    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();



        pballCount = 0;
        bookCount = 0;
        AddProject();
        TakeSeat();
        SetBallText();
        SetBookText();

    }

    void Awake()
    {
        Time.timeScale = 0f;
        if (teacherMovement.blah.isPlaying)
        {
            teacherMovement.blah.Stop();
        }
       
    }

    void Update()
    {

        if (Input.anyKeyDown)
        {
            Time.timeScale = 1f;
            panel.SetActive(false);
            isStart = true;
            
           
        }

        
        
        if (isSeated)
        {
            if (Input.GetKeyDown(KeyCode.Space) && tCount > 0)
            {

                if (paperballPrefab.activeSelf == true && pballCount > 0)
                {
                    GameObject paperball = Instantiate(paperballPrefab, shotSpawn.transform.position, paperballPrefab.transform.rotation);

                    Rigidbody paperballRB = paperball.GetComponent<Rigidbody>();
                    paperballRB.velocity = shotSpawn.forward * shotSpeed;

                    pballCount = pballCount - 1;
                    SetBallText();
                    AudioPlaying(throwball);

                }
                if (bookPrefab.activeSelf == true && bookCount > 0)
                {
                    GameObject bookball = Instantiate(bookPrefab, shotSpawn2.transform.position, bookPrefab.transform.rotation);

                    Rigidbody bookRB = bookball.GetComponent<Rigidbody>();
                    bookRB.velocity = shotSpawn2.forward * shotSpeed;
                    bookRB.angularVelocity = Vector3.forward;

                    bookCount = bookCount - 1;
                    SetBookText();
                    AudioPlaying(throwball);

                }

            }

            m_Rigidbody.velocity = Vector3.zero;
        }
        if (Input.GetKeyDown(KeyCode.F) && inArea == true)
        {

            if (isSeated == false)
            {
                Destroy(fCanvas.gameObject);
                TakeSeat();
            }
            else
            {
                GetUp();

            }
        }
    }

    void FixedUpdate()
    {
        

        if (isSeated)
        {

            if (Input.GetKey(KeyCode.A))
                transform.Rotate(Vector3.up * -turningSpeed * Time.deltaTime);
            if (Input.GetKey(KeyCode.D))
                transform.Rotate(Vector3.up * turningSpeed * Time.deltaTime);

            bool isWalking = false;
            m_Animator.SetBool("IsWalking", isWalking);

        }

        if (!isSeated)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            m_Movement.Set(horizontal, 0f, vertical);
            m_Movement.Normalize();


            bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
            bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
            bool isWalking = hasHorizontalInput || hasVerticalInput;
            m_Animator.SetBool("IsWalking", isWalking);


            if (isWalking)
            {
                // Give the player footstep audio as long as they continue to move
                if (!walk.isPlaying)
                {
                    walk.Play();
                }
            }
            else
            {
                walk.Stop();
            }
            
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
        m_Rigidbody.angularVelocity = Vector3.zero;
        

    }


    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * 0.05f);

        if (!isSeated)
            m_Rigidbody.MoveRotation(m_Rotation);

    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("PaperPickUp"))
        {

            Destroy(other.gameObject);
            pballCount = pballCount + 10;

            AddProject();
            SetBallText();
            AudioPlaying(paperPU);

        }

        if (other.gameObject.CompareTag("BookPickUp"))
        {

            Destroy(other.gameObject);
            bookCount = bookCount + 5;

            AddProject();
            SetBookText();
            AudioPlaying(bookPU);

        }

    }

   

    void AddProject()
    {
        tCount = bookCount + pballCount;


    }



    void TakeSeat()
    {

        isSeated = true;
        m_Rigidbody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        SnapBack();
        if (!squeak.isPlaying)
        {
            squeak.Play();
        }
        Material[] mats = new Material[] {InSeat, InSeat };
        seatrender.materials = mats;
         m_camera.m_Lens.FieldOfView = 35f;
        m_camera.Follow = looker;
        walk.Stop();
        
        



    }



    void SnapBack()
    {
        var rotationVector = transform.rotation.eulerAngles;
        rotationVector.y = 0;
        transform.rotation = Quaternion.Euler(rotationVector);
        transform.position = seatedTransform.position;
        m_Rigidbody.velocity = Vector3.zero;

    }


    void GetUp()
    {
        isSeated = false;
        m_Rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        if (fCanvas != null)
        {
            fCanvas.SetActive(false);
        }
        Material[] mats = new Material[]{OutSeat, OutSeat};
        seatrender.materials = mats;
        m_camera.m_Lens.FieldOfView = 40f;
        m_camera.Follow = head;
    }

    void FButton()
    {
        if (fCanvas != null)
        {
            fCanvas.SetActive(true);
        }
    }

    void SetBallText()
    {
        ballcountText.text = "x" + pballCount.ToString();
    }
    void SetBookText()
    {
        bookcountText.text = "x" + bookCount.ToString();
    }

    void AudioPlaying(AudioSource audioSource)
    {
        audioSource.Play();
    }


}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnitchMovement : MonoBehaviour
{
    [SerializeField] protected Vector3 m_from = new Vector3(0.0f, 0.0f, 0.0f);
    [SerializeField] protected Vector3 m_to = new Vector3(0.0f, 180.0f, 0.0f);
    [SerializeField] protected float m_frequency = 0.5f;
    public SnitchScript snitchScript;
    public TeacherMovement teacherMovement;
    public GameObject cone;
    
    // Start is called before the first frame update
    void Start()
    {
       
    }

     
    // Update is called once per frame
    void Update()
    {
        if (teacherMovement.isScanning == false && Time.timeScale == 1f)
        {
            MoveSnitch();
        }
        if (teacherMovement.isScanning == true)
        {
            PayAttention();
        }
    }

    void PayAttention()
    {
        var rotationVector = transform.rotation.eulerAngles;
        rotationVector.y = 0;
        transform.rotation = Quaternion.Euler(rotationVector);
        cone.SetActive(false);
    }
    void MoveSnitch()
    {
        cone.SetActive(true);
        Quaternion from = Quaternion.Euler(this.m_from);
        Quaternion to = Quaternion.Euler(this.m_to);

        float lerp = 0.5f * (1.0f + Mathf.Sin(Mathf.PI * Time.realtimeSinceStartup * this.m_frequency));

        this.transform.localRotation = Quaternion.Lerp(from, to, lerp);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjSelected : MonoBehaviour
{
    public int selectedProjectile = 1;
    // Start is called before the first frame update
    void Start()
    {
        SelectWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        
        {
            int previousSelectedProjectile = selectedProjectile;
            if (Input.GetKeyDown(KeyCode.E))

            {
                if (selectedProjectile >= transform.childCount - 1)
                    selectedProjectile = 0;
                else
                    selectedProjectile++;
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (selectedProjectile <= 0)
                    selectedProjectile = transform.childCount - 1;
                else
                    selectedProjectile--;
            }

            if (previousSelectedProjectile != selectedProjectile)
            {
                SelectWeapon();
            }
        }
    }

    void SelectWeapon()
    {
        int i = 0;
        foreach (Transform projectile in transform)
        {
            if (i == selectedProjectile)
                projectile.gameObject.SetActive(true);
            else
                projectile.gameObject.SetActive(false);
            i++;
        }
    }

}

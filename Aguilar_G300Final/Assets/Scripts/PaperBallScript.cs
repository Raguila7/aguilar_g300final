﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperBallScript : MonoBehaviour
{


    SphereCollider m_sphereCollider;
    Rigidbody m_rigidbody;
    public TeacherRage teacherRage;
    private Material mymat;
    public AudioSource hit;


    // Start is called before the first frame update
    void Start()
    {
        m_sphereCollider = GetComponent<SphereCollider>();
        mymat = GetComponentInChildren<Renderer>().material;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

   void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Floor"))
        {
            
            Destroy(gameObject, 2f);
        }
        if (other.gameObject.CompareTag("Teacher"))
        {
            TeacherRage eHealth = other.gameObject.GetComponent<TeacherRage>();
            hit.Play();

            if (eHealth != null)
            {
                eHealth.TakeDamage(1);
                teacherRage.SetSlider();
                mymat.SetColor("_EmissionColor", Color.red);
                mymat.EnableKeyword("_EMISSION");


            }
           
        }
    }
}

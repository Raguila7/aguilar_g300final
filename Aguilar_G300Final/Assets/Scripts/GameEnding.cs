﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{

    public float fadeDuration = 2f;
    public GameObject player;
    public PlayerMovement playerMovement;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public CanvasGroup snitchBackgroundImageCanvasGroup;
    public CanvasGroup winBackgroundImageCanvasGroup;
    public float displayImageDuration = 4f;
    public float displayWImageDuration = 7f;
    public GameObject pOV;
    public TeacherMovement teacherMovement;
    
    public AudioSource awwAudio;
    public AudioSource caughtAudio;
    public AudioSource winAudio;
   


    bool m_IsPlayerCaught;
    bool m_IsPlayerCaughtS;
    bool m_IsPlayerGood;
    float m_Timer;
    // Start is called before the first frame update
    void Start()
    {
       
        
    }
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
        
    }

    public void SnitchPlayer()
    {
        m_IsPlayerCaughtS = true;
    }
    public void PassGrade()
    {
        m_IsPlayerGood = true;
    }




    // Update is called once per frame
    void Update()
    {
        if (m_IsPlayerCaught)
        {
            EndGame(caughtBackgroundImageCanvasGroup, true);
        }
        if (GameObject.Find("Lewis").GetComponent<PlayerMovement>().isSeated == false && teacherMovement.isScanning == true)
        {
            EndGame(caughtBackgroundImageCanvasGroup, true);
              
            if (!awwAudio.isPlaying)
            {awwAudio.Play();
            }

        }
        if(m_IsPlayerCaughtS)
        {
            EndGame(snitchBackgroundImageCanvasGroup, true);
            if (!caughtAudio.isPlaying)
            {
                caughtAudio.Play();
            }

        }
        if (m_IsPlayerGood)
        {
            WinGame(winBackgroundImageCanvasGroup, true);
            if (!winAudio.isPlaying)
            {
                winAudio.Play();
            }

        }
    }

    void EndGame(CanvasGroup imageCanvasGroup, bool doRestart)
    {
        
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        playerMovement.enabled = false;
        teacherMovement.scan.Pause();
        teacherMovement.blah.Pause();
        playerMovement.music.Pause();
        playerMovement.walk.Pause();
        
        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }

    void WinGame(CanvasGroup imageCanvasGroup, bool nextLevel)
    {
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        
        playerMovement.music.Pause();
        teacherMovement.scan.Pause();
        teacherMovement.blah.Pause();

        if (m_Timer > fadeDuration + displayWImageDuration)
        {
            if (nextLevel)
            {
                SceneManager.LoadScene("Level 2");
            }
            else
            {
                Application.Quit();
            }
        }
    }

 
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeacherVision : MonoBehaviour
{

    public Transform player;
    bool m_IsBallInRange;
    public GameEnding gameEnding;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_IsBallInRange)
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                    gameEnding.CaughtPlayer();
                }
            }
        }
    }
   void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "PaperBallProjectile(Clone)")
        {
            m_IsBallInRange = true;
            
        }
        if (other.transform.name == "BookProjectile(Clone)")
        {
            m_IsBallInRange = true;

        }

    }
    void OnTriggerExit(Collider other)
    {
        if (other.transform.name == "PaperBallProjectile(Clone)")
        {
            m_IsBallInRange = false;
        }
        if (other.transform.name == "BookProjectile(Clone)")
        {
            m_IsBallInRange = false;

        }
    }
}

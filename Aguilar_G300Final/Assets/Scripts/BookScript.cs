﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookScript : MonoBehaviour
{


    BoxCollider m_boxCollider;
    Rigidbody m_rigidbody;
    public TeacherRage teacherRage;
    private Material mymat;
    public AudioSource hit;

    // Start is called before the first frame update
    void Start()
    {
        m_boxCollider = GetComponent<BoxCollider>();
        mymat = GetComponentInChildren<Renderer>().material;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Floor"))
        {

            Destroy(gameObject, 2f);

        }
        if (other.gameObject.CompareTag("Teacher"))
        {
            TeacherRage eHealth = other.gameObject.GetComponent<TeacherRage>();
            hit.Play();

            if (eHealth != null)
            {
                eHealth.TakeDamage(2);
                teacherRage.SetSlider();
                mymat.SetColor("_EmissionColor", Color.red);
                mymat.EnableKeyword("_EMISSION");
                Destroy(gameObject, 0.5f);
            }
        }
    }
}
